
import java.util.Scanner;

import mcq.Choice;
import mcq.AssessmentFormLoader;
import mcq.Question;
import mcq.AssessmentForm;
import util.PRNG;
import java.util.Random;

/**
 * Un simple simulateur de questionnaire à choix multiple fonctionnant en mode console
 * Vous pouvez modifier ce fichier pour votre projet ou vous en inspirer librement.
 * Comme vous pouvez voir nous fournissons à titre d'exemple une simple methode d'évaluation (simpleGrader).
 * Les questions sont toujours présentées dans le même ordre ainsi que les propositions de réponses.
 * N'hésitez pas à implémenter d'autres évaluateurs et ajouter de l'aléatoire dans la présentation/selection des questions
 * et des choix.
 * @author (P. Schaus pour la structure) - Oliva Justin - Maris Lucas - Lemer Guillaume (- Beigadeh Amir)
 */
public final class AssessmentFormConsole {

    /**
     *
     * @pre -
     * @post implémentation de l'utilisation de la fonction d'aléatoire
     *       permet d'appeler le nombre aléatoire suivant dont la valeur max est choisie.
     *
     */

    /**
    public static int nextInt() {
    int max = 1;
    int myseed = 5;
    Random prng = new Random();
    prng.setSeed(myseed);
    return prng.nextInt(1);
    }
     */

    /**
     *
     * @pre Question[] != null
     * @param Question[] questions
     * @return Question[] questions
     * @post methode de melange des questions
     *       retourne le tableau donné mélangé
     *
     */

    public static Question[] shuffleQ(Question[] questions ) {
        for (int i = 0; i < questions.length; i++) {
            int moveTo = (int) Math.floor(Math.random()*questions.length);
            Question switcheroo = questions[i];
            questions[i] = questions[moveTo];
            questions[moveTo] = switcheroo;
        }
        return questions;
    }

    /**
     *
     * @pre Question[] != null
     * @param Choice[] choices
     * @return Choice[] choices
     * @post methode de melange des reponses
     *       retourne le tableau donné mélangé
     *
     */

    public static Choice[] shuffleC(Choice[] choices ) {
        for (int i = 0; i < choices.length; i++) {
            int moveTo = (int) Math.floor(Math.random()*choices.length);
            Choice switcheroo = choices[i];
            choices[i] = choices[moveTo];
            choices[moveTo] = switcheroo;
        }
        return choices;
    }
    /*
     * @pre -
     * @param -
     * @return -
     * @post paramètre la cotation du QCM
     *
     */

    public static int pointsBonneReponse;
    public static int pointsMauvaiseReponse;

    public static void parametrageCotation(){
        //On demande à l'utilisateur le mode de cotation souhaité
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println("Bonjour! Bienvenue! Veuillez spécifier le mode de cotation souhaité :");
        System.out.println("a) Cotation classique (+1 pour les bonnes réponses, 0 pour les mauvaises)");
        System.out.println("b) Cotation à l'américaine (+1 pour les bonnes réponses, -1 pour les mauvaises)");
        System.out.println("c) Cotation au choix (à paramétrer)");

        //On récupère son input, en vérifiant qu'il soit bien a, b ou c
        for(boolean valid = false; !valid; ) {
            Scanner nextchar = new Scanner(System.in);

            char choix = nextchar.next().charAt(0);

            // cotation classique
            if(choix == 'a') {
                pointsBonneReponse = 1;
                pointsMauvaiseReponse = 0;
                valid = true;
            }
            // cotation amériquaine
            else if(choix == 'b') {
                pointsBonneReponse = 1;
                pointsMauvaiseReponse = -1;
                valid = true;
            }
            // cotation perso
            else if(choix == 'c') {
                System.out.print("Veuillez entrer le gain/la perte de points si la réponse est bonne : ");
                pointsBonneReponse = Character.getNumericValue(nextchar.next().charAt(0));
                System.out.print("Veuillez entrer la perte/le gain de points si la réponse est mauvaise : ");
                pointsMauvaiseReponse = Character.getNumericValue(nextchar.next().charAt(0));
                valid = true;
            }
            else{
                System.out.print("Veuillez entrer un choix valide! ");
            }
        }

        System.out.println("Le système de cotation a été correctement paramétré!");
        System.out.println("-------------------------------------------------------------------------------");
        System.out.println();
    }

    /**
     * Evaluateur simple:
     * @param q est la qestion
     * @param choiceIdx est l'indice de la réponse choisie
     * @return on retourne la valeur correspond à une réponse vraie si la réponse est vraie, sinon la valeur correspondant à une réponse fausse
     */
    public static double simpleGrader(Question q, int choiceIdx) {
        // on teste si choiceIdx est bien un choix correct pour cette question
        if (q.isCorrectChoice(choiceIdx)) {
            return pointsBonneReponse;
        } else {
            return pointsMauvaiseReponse;
        }
    }

    /**
     * 1) Affiche la question dans la console et
     * 2) propose les réponses/choix possibles à cette question
     * 3) demande à l'utilisateur un choix parmi une ces réponse
     * @param q est la question à proposer à l'utilisateur
     * @return l'indice de la réponse faite par l'utilisateur après présentation de la question et des réponses possibles
     */

    public static int getAnswer(Question q) {
        // affiche la question
        System.out.println(q.getWording());
        // recuperation du tableau des choix possibles
        Choice [] choices = q.getChoices();
        //melange du string choices
        choices = shuffleC(choices);
        //affichage du string melange
        for (int i = 0; i < choices.length; i++) {
            Choice choice = choices[i];
            // affichage du choix à l'indice i
            System.out.println("   ["+i+"]\t"+choice); // des espaces et non un \t ca la tabulation est trop longue
        }

        System.out.print("Veuillez entrer une réponse et appuyer sur Enter ... ");
        char choosenChar;
        int choosenNumber = 0;
        // on demande à l'étudiant de taper au clavier sa réponse (un entier parmi les choix possibles)
        // ceci est equivalent à TextIO.getIntLn() vu au cours avec Olivier Bonaventure et Charles Pecheur
        boolean asw = false;
        while ( !asw ) {
            Scanner in = new Scanner(System.in);
            choosenChar = in.next().charAt(0);

            //vérification de lettre ou chiffre
            if (!Character.isDigit(choosenChar)){
                System.out.println ("Veuillez entrer une reponse valide ...");
            }
            // c est un chiffre
            else {
                choosenNumber = Character.getNumericValue(choosenChar);

                // est t il une des réponces
                if (choosenNumber < 0 || choosenNumber > choices.length - 1) {
                    System.out.println("Veuillez entrer une reponse valide ...");
                }
                else {
                    asw = true;
                }
            }
        }
        // affichage de la reponce et passage a la suivante
        System.out.println(choices[choosenNumber].getExplanation());
        return choosenNumber;
    }

    /**
     *
     * @pre -
     * @post retourne le nom du fichier a lire et/ou ajoute l'extenssion .txt si nécessaire
     * @param -
     * @return filename
     *
     */
    public static String choixdufichier(){
        String filename="";
        String extenssion="";

        // demande entrer nom du fichier avec ou sans le .txt
        System.out.println (" veuillez entrer le nom du fichier a lire pour le QCM ");
        for (boolean valide = false; !valide; ) {
            Scanner file = new Scanner(System.in);
            filename = file.nextLine();

            // verification que un nom est bien rentré
            if (filename != null) {
                System.out.println("Votre fichier "+ filename + " vas etre utiliser pour générer le QCM.");
                valide = true;
            }
            else {
                System.out.println("veuillez enter un nom de fichier...");

            }
        }

        // le fichier a t il l extenssion .txt ?
        if ( filename.length() > 4) {
            extenssion = filename.substring(filename.length()-4);
        }
        else{
            extenssion = null;
        }
        // ajout de l'extenssion si nécessaire
        if (extenssion == null || extenssion != ".txt") {
            filename += ".txt";
            System.out.println(" (le fichier "+ filename+ " sera donc utilisé...)");
        }
        // retour du nom de fichier formaté pour lecture
        return filename;
    }

    /**
     *
     * @pre -
     * @post methode main
     *
     */

    public static void main(String[] args) {

        // choix du fichier
        String filename = choixdufichier();
        //Paramétrage du système de cotation
        parametrageCotation();
        // Chargement du QCM depuis le fichier (questions et réponses aux questions)
        AssessmentForm mcq = AssessmentFormLoader.buildQuestionnaire(filename);
        // Recuperation du tableau des questions melangees
        Question [] questions = mcq.getQuestions();
        //Il faut mélanger
        questions = shuffleQ(questions);
        //initialisation variable score
        double totalScore = 0.0;
        // On itère sur chaque question pour evalue le score de l'étudiant à cette question
        for (int i = 0; i < questions.length; i++) {
            Question q = questions[i];
            totalScore += simpleGrader(q,getAnswer(q));
            System.out.println();
        }
        // affichage du score final
        System.out.println("-----------------------------------------------------");
        System.out.println("Votre score est de " + totalScore + " sur un total de " + questions.length + " questions.");
        System.out.println("-----------------------------------------------------");

    }

}
