
----------------------------
README AssessmentFromConsole
----------------------------

But de notre project:
   - Mélange des question et des réponces données dans un fichier .txt. Ainsi que mise en place d'un systeme de cotation.

Version:
  - 10 novembre 2017.

Comment lancer notre programme:
  - Placer le fichier AssessmentFormConsole.java dans BlueJ.
  - Lancer BlueJ et click droit sur AssessmentFormConsole.java afin de le compiler.
  - Click droit sur AssessmentFormConsole.java et clicker sur void main().
  - Suivre les instructions.

Auteurs:
  - Oliva Justin
  - Maris Lucas
  - Lemer Guillaume
