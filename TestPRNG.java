//------------------------------
// Test de prng
//------------------------------
public static boolean isCorrect(PRNGFactory factory) {
        // you can create a PRNG with seed=33
        // that generates number on the interval [0..limit-1] like this
        // PRNG prng = factory.getPRNG(33,10);
        // to get the next random number:
        // prng.nextInt()
        // return true;

        if (test_recurrence(factory) && test_nombre(factory) && test_sequence(factory) && test_seed(factory)) {
                return true;
        } else {
                return false;
        }
}
//------------------------------
// différents tests sur le prng
//------------------------------
//
// va tester si un nombre ne revient pas trop de fois d'affilée
//
public static boolean test_recurrence(PRNGFactory factory) {
  boolean correct = true;
  for (int Myseed = 0; Myseed <= 10 && correct;Myseed++){
        PRNG prng = factory.getPRNG(Myseed,10);
        int count = 0;
        for (int i = 1; i < 10000; i++) { // vérifie pour les 10.000 premiers nombres
                int a = prng.nextInt();
                int b = prng.nextInt();
                if (a == b) { // si 2 nombres sont identiques l'un apres l'autre
                        count++;
                } else {
                        count = 0;
                }

                if (count == 25) { // si on a 25 nombres identiques l'un apres l'autre
                        correct = false;
                }
        }
    }
    return correct;
}
//
// va tester si tous les nombres sont présents un nombre (à peu près) identique de fois
//
public static boolean test_nombre(PRNGFactory factory) {
  boolean correct = true;
  for (int Myseed = 0; Myseed <= 10 && correct;Myseed++){
        PRNG prng = factory.getPRNG(Myseed,10);
        int nbreDe0 = 0;
        int nbreDe1 = 0;
        int nbreDe2 = 0;
        int nbreDe3 = 0;
        int nbreDe4 = 0;
        int nbreDe5 = 0;
        int nbreDe6 = 0;
        int nbreDe7 = 0;
        int nbreDe8 = 0;
        int nbreDe9 = 0;

        for (int i = 1; i < 10000; i++) // vérifie pour les 10.000 premiers nombres
        {
                switch (prng.nextInt()) {
                case 0:
                        nbreDe0++;
                        break;
                case 1:
                        nbreDe1++;
                        break;
                case 2:
                        nbreDe2++;
                        break;
                case 3:
                        nbreDe3++;
                        break;
                case 4:
                        nbreDe4++;
                        break;
                case 5:
                        nbreDe5++;
                        break;
                case 6:
                        nbreDe6++;
                        break;
                case 7:
                        nbreDe7++;
                        break;
                case 8:
                        nbreDe8++;
                        break;
                case 9:
                        nbreDe9++;
                        break;
                }
        }
        // s'ils sur 10.000 fois les 10 options sortent à peu près mille fois chacune :
        if ((nbreDe0 >= 900 && nbreDe0 <= 1100) && (nbreDe1 >= 900 && nbreDe1 <= 1100) && (nbreDe2 >= 900 && nbreDe2 <= 1100) && (nbreDe3 >= 900 && nbreDe3 <= 1100) && (nbreDe4 >= 900 && nbreDe4 <= 1100) && (nbreDe5 >= 900 && nbreDe5 <= 1100) && (nbreDe6 >= 900 && nbreDe6 <= 1100) && (nbreDe7 >= 900 && nbreDe7 <= 1100) && (nbreDe8 >= 900 && nbreDe8 <= 1100) && (nbreDe9 >= 900 && nbreDe9 <= 1100)) {
                correct = true;
        } else {
                correct = false;
        }
      }
    return correct;
}
//
// va tester si on ne retrouve pas de séquences de nombres identiques
//
public static boolean test_sequence(PRNGFactory factory) {
  boolean correct = true;
  for (int Myseed = 0; Myseed <= 10 && correct;Myseed++){
        PRNG prng = factory.getPRNG(Myseed,10);
        int a = prng.nextInt();
        boolean find = false;
        String suiteA = "-";
        String suiteB = "-";
        int count = 0;
        int count2 = 0;

        for (int i = 1; !find; i++) { // création de la 1er suite
                int b = prng.nextInt();
                if (a == b || count == 10000) {
                        find = true; // fin de la 1er suite
                } else {
                        suiteA = suiteA.concat(String.valueOf(b)); //remplissage suite 1
                        count++;
                }
        }

        boolean find2 = false;
        for (int i = 1; !find2; i++) { // création de la 2e suite
                int b = prng.nextInt();
                if (a == b || count == 10000) {
                        find2 = true; // fin de la 2e suite
                } else {
                        suiteB = suiteB.concat(String.valueOf(b)); //remplissage suite 2
                        count2 ++;
                }
        }

        if (suiteA.equals(suiteB) || count == 10000) { // vérifie si les 2 suites sont identiques
                correct = false;
        }
      }
    return correct;
}
public static boolean test_seed(PRNGFactory factory){
    String value ="";
    String value2 ="";
      PRNG prng = factory.getPRNG(0,10);
    for (int i = 0; i <10; i++){
      value += prng.nextInt();
    }
          PRNG prng2 = factory.getPRNG(2,10);
    for (int i = 0; i <10; i++){
      value2 += prng2.nextInt();
    }
    if (value.equals(value2)){
        return false;
    }
    return true;
}
